package pl.edu.pb.projekt;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Feed {
    @PrimaryKey(autoGenerate = true)
    public int fid;

    @ColumnInfo(name = "url")
    public String url;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "author")
    public String author;

    @Ignore
    public Feed(int fid, String url, String description, String author) {
        this.fid = fid;
        this.url = url;
        this.description = description;
        this.author = author;
    }

    public Feed(String url, String description, String author) {
        this.url = url;
        this.description = description;
        this.author = author;
    }
}