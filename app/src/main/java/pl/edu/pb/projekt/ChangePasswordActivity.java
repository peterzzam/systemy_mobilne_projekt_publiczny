package pl.edu.pb.projekt;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordActivity extends AppCompatActivity {

    private String email;
    private EditText oldUserPassword;
    private EditText newUserPassword;
    private EditText newUserPasswordRepeated;
    private Button changePasswordButton;
    private Button cancelButton;
    private BottomNavigationView bottomNavigationView;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private AuthCredential authCredential;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        oldUserPassword = (EditText) findViewById(R.id.old_user_password);
        newUserPassword = (EditText) findViewById(R.id.new_user_password);
        newUserPasswordRepeated = (EditText) findViewById(R.id.new_user_password_repeated);
        changePasswordButton = (Button) findViewById(R.id.change_password_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        email = firebaseUser.getEmail();

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Prompt the user to re-provide their sign-in credentials
                if ((!TextUtils.isEmpty(newUserPassword.getText().toString()) && newUserPassword.getText().toString().length() > 7
                        || !TextUtils.isEmpty(newUserPasswordRepeated.getText().toString()) && newUserPasswordRepeated.getText().toString().length() > 7)
                        && (newUserPassword.getText().toString().equals(newUserPasswordRepeated.getText().toString()))) {
                    authCredential = EmailAuthProvider.getCredential(email, oldUserPassword.getText().toString());
                    firebaseUser.reauthenticate(authCredential)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        firebaseUser.updatePassword(newUserPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                            }
                                        });
                                    }
                                }
                            });
                    finish();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void givemesometoast(EditText etText,String s) {
        etText.setError(s);
        etText.requestFocus();
    }
}