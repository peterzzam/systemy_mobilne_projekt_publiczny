package pl.edu.pb.projekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchActivity extends AppCompatActivity {

    private SearchView searchView;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;

    private ProfileModal profileModal;
    private ArrayList<FeedModal> feedModalArrayList;

    private AppDatabase appDatabase;
    private List<Feed> feeds;
    private FeedModal feedModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchView = findViewById(R.id.search_bar);
        bottomNavigationView = findViewById(R.id.bottom_bar);
        feedModalArrayList = new ArrayList<>();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            // Override onQueryTextSubmit method which is call when submit query is searched
            @Override
            public boolean onQueryTextSubmit(String query) {
                feedModalArrayList.clear();
                appDatabase = AppDatabase.getInstance(getApplicationContext());
                feeds = appDatabase.feedDao().getAll();
                for (Feed feed : feeds) {
                    if (feed.description.contains(query)) {
                        feedModal = new FeedModal(feed.fid,
                                appDatabase.userDao().findByName(feed.author).userAvatar,
                                appDatabase.userDao().findByName(feed.author).userName,
                                feed.url,
                                feed.description);
                        feedModalArrayList.add(feedModal);
                    }
                }
                if(feedModalArrayList.isEmpty()) {
                    Toast.makeText(SearchActivity.this, "Not found", Toast.LENGTH_LONG).show();
                }
                getData();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        finish();
                        break;
                    case R.id.add:
                        intent = new Intent(bottomNavigationView.getContext(), AddActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.profile:
                        intent = new Intent(bottomNavigationView.getContext(), ProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });
    }

    private void getData() {
        FeedAdapter feedAdapter = new FeedAdapter(feedModalArrayList, SearchActivity.this);
        RecyclerView feedItemsView = findViewById(R.id.feed_items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchActivity.this, RecyclerView.VERTICAL, false);

        feedItemsView.setLayoutManager(linearLayoutManager);

        feedItemsView.setAdapter(feedAdapter);
    }
}