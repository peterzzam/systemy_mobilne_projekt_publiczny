package pl.edu.pb.projekt;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

//    private Button registerButton;
//    private Button cancelButton;

    private EditText userEmail;
    private EditText userPassword;
    private EditText userPasswordRepeated;
    private Button registerButton;
    private Button cancelButton;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userEmail = (EditText) findViewById(R.id.user_email);
        userPassword = (EditText) findViewById(R.id.user_password);
        userPasswordRepeated = (EditText) findViewById(R.id.user_password_repeated);
        registerButton = (Button) findViewById(R.id.register_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        firebaseAuth = FirebaseAuth.getInstance();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = userEmail.getText().toString().trim();
                String password = userPassword.getText().toString().trim();
                String passwordRepeated = userPasswordRepeated.getText().toString().trim();

                if (!email.contains("@") && TextUtils.isEmpty(email) && email.length()<7) {
                    givemesometoast(userEmail,"Invalid email.. ");
                } else if (!TextUtils.isEmpty(password) && password.length() > 7 || !TextUtils.isEmpty(passwordRepeated) && passwordRepeated.length()>7 ) {
                    //Do your stuffs here.
                    if (password.equals(passwordRepeated) && password.length() >= 7) {
                        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @RequiresApi (api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful())
                                {
                                    finish();
                                    Pair[] pairs=new Pair[1];
                                    pairs[0]=new Pair<View,String>(userEmail, "etTransition");

                                    AppDatabase appDatabase = AppDatabase.getInstance(getApplicationContext());
                                    User user = new User("https://www.w3schools.com/howto/img_avatar.png", email);
                                    appDatabase.userDao().insertAll(user);

                                    Intent intent = new Intent(view.getContext(), LoggedOutActivity.class);
                                    view.getContext().startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                        //  Toast.makeText(RegisterActivity.this, "Correct credentials", Toast.LENGTH_SHORT).show();

                        //givemesometoast("Your credentials input are correct...");
                    }else {
                        givemesometoast(userPassword,"Passwords dont match...");

                    }

                }else{
                    givemesometoast(userPassword,"Invalid passwords ");
                }



            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LoggedOutActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });
    }

    private void givemesometoast(EditText eText,String s) {
        eText.setError(s);
        eText.requestFocus();
    }
}