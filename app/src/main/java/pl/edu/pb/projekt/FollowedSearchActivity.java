package pl.edu.pb.projekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowedSearchActivity extends AppCompatActivity {

    private SearchView searchView;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;

    private ArrayList<UserModal> userModalArrayList;

    private AppDatabase appDatabase;
    private List<User> users;
    private UserModal userModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followed_search);

        searchView = findViewById(R.id.search_bar);
        bottomNavigationView = findViewById(R.id.bottom_bar);
        userModalArrayList = new ArrayList<>();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            // Override onQueryTextSubmit method which is call when submit query is searched
            @Override
            public boolean onQueryTextSubmit(String query) {
                userModalArrayList.clear();
                appDatabase = AppDatabase.getInstance(getApplicationContext());
                users = appDatabase.userDao().getAll();
                for (User user : users) {
                    if (user.userName.contains(query)) {
                        userModal = new UserModal(user.uid,
                                user.userAvatar,
                                user.userName);
                        userModalArrayList.add(userModal);
                    }
                }
                if(userModalArrayList.isEmpty()) {
                    Toast.makeText(FollowedSearchActivity.this, "Not found", Toast.LENGTH_LONG).show();
                }
                getData();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        intent = new Intent(bottomNavigationView.getContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.add:
                        intent = new Intent(bottomNavigationView.getContext(), AddActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.profile:
                        break;
                }
                return true;
            }
        });
    }

    private void getData() {
        UserAdapter adapter = new UserAdapter(userModalArrayList, FollowedSearchActivity.this);
        RecyclerView userItemsView = findViewById(R.id.user_items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FollowedSearchActivity.this, RecyclerView.VERTICAL, false);

        userItemsView.setLayoutManager(linearLayoutManager);

        userItemsView.setAdapter(adapter);
    }
}