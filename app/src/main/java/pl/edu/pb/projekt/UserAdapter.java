package pl.edu.pb.projekt;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private ArrayList<UserModal> userModalArrayList;
    private Context context;

    private AppDatabase appDatabase;
    private FirebaseUser firebaseUser;
    private Followed followed;

    public UserAdapter(ArrayList<UserModal> userModalArrayList, Context context) {
        this.userModalArrayList = userModalArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // inflating our layout for item of recycler view item.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_user_item, parent, false);
        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, int position) {
        UserModal userModal = userModalArrayList.get(position);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        appDatabase = AppDatabase.getInstance(context.getApplicationContext());
        Picasso.get().load(userModal.getUser_avatar()).into(holder.user_avatar);
        holder.user_name.setText(userModal.getUser_name());
        if(appDatabase.followedDao().findByName(userModal.getUser_name(), firebaseUser.getEmail()) == null) {
            holder.unfollow_follow_button.setText(R.string.start_following);
        } else {
            holder.unfollow_follow_button.setText(R.string.stop_following);
        }
        Log.d(null, holder.unfollow_follow_button.getText().toString());
        holder.unfollow_follow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.unfollow_follow_button.getText().toString().equals("Follow")) {
                    ((Button) view).setText(R.string.stop_following);
                    followed = new Followed(userModal.getUser_name(), firebaseUser.getEmail());
                    appDatabase.followedDao().insertAll(followed);
                } else {
                    ((Button) view).setText(R.string.start_following);
                    followed = appDatabase.followedDao().findByName(userModal.getUser_name(), firebaseUser.getEmail());
                    appDatabase.followedDao().delete(followed);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return userModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_avatar;
        private TextView user_name;
        private Button unfollow_follow_button;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user_avatar = itemView.findViewById(R.id.user_avatar);
            user_name = itemView.findViewById(R.id.user_name);
            unfollow_follow_button = itemView.findViewById(R.id.unfollow_follow_button);
        }
    }
}
