package pl.edu.pb.projekt;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity
public class Followed {
    @PrimaryKey(autoGenerate = true)
    public int fid;

    @ColumnInfo(name = "followed_name")
    public String followedName;

    @ColumnInfo(name = "follower_name")
    public String followerName;

    @Ignore
    public Followed(int fid, String followedName, String followerName) {
        this.fid = fid;
        this.followedName = followedName;
        this.followerName = followerName;
    }

    public Followed(String followedName, String followerName) {
        this.followedName = followedName;
        this.followerName = followerName;
    }
}