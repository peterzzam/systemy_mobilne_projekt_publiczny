package pl.edu.pb.projekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class FollowedActivity extends AppCompatActivity {

    private ImageButton searchButton;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;
    private ArrayList<UserModal> userModalArrayList;

    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followed);

        searchButton = (ImageButton) findViewById(R.id.search_button);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_bar);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), FollowedSearchActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        break;
                    case R.id.add:
                        intent = new Intent(bottomNavigationView.getContext(), AddActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.profile:
                        intent = new Intent(bottomNavigationView.getContext(), ProfileActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        userModalArrayList = new ArrayList<>();

        getData();
    }

    private void getData() {

        AppDatabase appDatabase = AppDatabase.getInstance(getApplicationContext());
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        List<Followed> followedList = null;
        if(appDatabase.followedDao().getFollowed(firebaseUser.getEmail()) != null) {
            followedList = appDatabase.followedDao().getFollowed(firebaseUser.getEmail());
        }

        UserModal userModal;

        for(Followed followed : followedList) {
            User user = appDatabase.userDao().findByName(followed.followedName);
            userModal = new UserModal(user.uid, user.userAvatar, user.userName);
            userModalArrayList.add(userModal);
        }

        UserAdapter adapter = new UserAdapter(userModalArrayList, FollowedActivity.this);
        RecyclerView userItemsView = findViewById(R.id.user_items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FollowedActivity.this, RecyclerView.VERTICAL, false);

        userItemsView.setLayoutManager(linearLayoutManager);

        userItemsView.setAdapter(adapter);
    }
}