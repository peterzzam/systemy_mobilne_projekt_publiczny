package pl.edu.pb.projekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private Button settingsButton;
    private Button followedButton;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;

    private ProfileModal profileModal;
    private ArrayList<FeedModal> feedModalArrayList;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        settingsButton = (Button) findViewById(R.id.settings_button);
        followedButton = (Button) findViewById(R.id.followed_button);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_bar);

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), SettingsActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        followedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), FollowedActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        intent = new Intent(bottomNavigationView.getContext(), HomeActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.add:
                        intent = new Intent(bottomNavigationView.getContext(), AddActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.profile:
                        break;
                }
                return true;
            }
        });

        feedModalArrayList = new ArrayList<>();

        getData();
    }

    private void getData() {
        AppDatabase appDatabase = AppDatabase.getInstance(getApplicationContext());
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        User user = appDatabase.userDao().findByName(firebaseUser.getEmail());

        profileModal = new ProfileModal(user.uid, user.userAvatar, user.userName);

        CircleImageView profile_avatar = (CircleImageView)findViewById(R.id.profile_avatar);
        Picasso.get().load(profileModal.getProfile_avatar()).into(profile_avatar);
        TextView profile_name = (TextView)findViewById(R.id.profile_name);
        profile_name.setText(profileModal.getProfile_name());

        List<Feed> feeds = appDatabase.feedDao().getAll();

        for (Feed feed : feeds) {
            FeedModal feedModal = new FeedModal(feed.fid,
                    appDatabase.userDao().findByName(feed.author).userAvatar,
                    appDatabase.userDao().findByName(feed.author).userName,
                    feed.url,
                    feed.description);
            feedModalArrayList.add(feedModal);
        }

        FeedAdapter feedAdapter = new FeedAdapter(feedModalArrayList, ProfileActivity.this);
        RecyclerView feedItemsView = findViewById(R.id.feed_items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProfileActivity.this, RecyclerView.VERTICAL, false);

        feedItemsView.setLayoutManager(linearLayoutManager);

        feedItemsView.setAdapter(feedAdapter);
    }
}