package pl.edu.pb.projekt;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "user_avatar")
    public String userAvatar;

    @ColumnInfo(name = "user_name")
    public String userName;

    @Ignore
    public User(int uid, String userAvatar, String userName) {
        this.uid = uid;
        this.userAvatar = userAvatar;
        this.userName = userName;
    }

    public User(String userAvatar, String userName) {
        this.userAvatar = userAvatar;
        this.userName = userName;
    }
}