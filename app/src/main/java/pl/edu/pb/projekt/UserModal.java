package pl.edu.pb.projekt;

public class UserModal {
    // variables for storing data
    // of our recycler view item
    private int id;
    private String user_avatar;
    private String user_name;

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public UserModal(int id, String user_avatar, String user_name) {
        this.id = id;
        this.user_avatar = user_avatar;
        this.user_name = user_name;
    }
}