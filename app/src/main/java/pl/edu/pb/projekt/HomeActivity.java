package pl.edu.pb.projekt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private ImageButton searchButton;
    private ImageButton ideaButton;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;
    private ArrayList<FeedModal> feedModalArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        searchButton = (ImageButton) findViewById(R.id.search_button);
        ideaButton = (ImageButton) findViewById(R.id.idea_button);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_bar);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), SearchActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        ideaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), IdeaActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        break;
                    case R.id.add:
                        intent = new Intent(bottomNavigationView.getContext(), AddActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.profile:
                        intent = new Intent(bottomNavigationView.getContext(), ProfileActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        feedModalArrayList = new ArrayList<>();

        getData();
    }

    private void getData() {
        FeedModal feedModal = new FeedModal(0,
                "https://www.w3schools.com/howto/img_avatar.png",
                "Twój stary",
                "https://upload.wikimedia.org/wikipedia/commons/e/e7/Everest_North_Face_toward_Base_Camp_Tibet_Luca_Galuzzi_2006.jpg",
                "No ładna górka nie");

        FeedModal feedModal2 = new FeedModal(1,
                "https://www.w3schools.com/howto/img_avatar.png",
                "Twój stary",
                "https://upload.wikimedia.org/wikipedia/commons/e/e7/Everest_North_Face_toward_Base_Camp_Tibet_Luca_Galuzzi_2006.jpg",
                "No ładna górka nie");

        feedModalArrayList.add(feedModal);
        feedModalArrayList.add(feedModal2);

        FeedAdapter adapter = new FeedAdapter(feedModalArrayList, HomeActivity.this);
        RecyclerView feedItemsView = findViewById(R.id.feed_items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.VERTICAL, false);

        feedItemsView.setLayoutManager(linearLayoutManager);

        feedItemsView.setAdapter(adapter);
    }
}