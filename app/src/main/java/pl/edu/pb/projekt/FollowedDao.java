package pl.edu.pb.projekt;

import androidx.annotation.Nullable;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FollowedDao {
    @Query("SELECT * FROM followed")
    List<Followed> getAll();

    @Query("SELECT * FROM followed WHERE fid IN (:fids)")
    List<Followed> loadAllByIds(int[] fids);

    @Query("SELECT * FROM followed WHERE followed_name LIKE :followedName")
    List<Followed> getFollowers(String followedName);

    @Query("SELECT * FROM followed WHERE follower_name LIKE :followerName")
    List<Followed> getFollowed(String followerName);

    @Query("SELECT * FROM followed WHERE followed_name LIKE :followedName AND follower_name LIKE :followerName LIMIT 1")
    @Nullable
    Followed findByName(String followedName, String followerName);

    @Insert
    void insertAll(Followed... followees);

    @Delete
    void delete(Followed followed);
}