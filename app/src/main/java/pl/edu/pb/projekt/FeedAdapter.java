package pl.edu.pb.projekt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private ArrayList<FeedModal> feedModalArrayList;
    private Context context;

    public FeedAdapter(ArrayList<FeedModal> feedModalArrayList, Context context) {
        this.feedModalArrayList = feedModalArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // inflating our layout for item of recycler view item.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_feed_item, parent, false);
        return new FeedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedAdapter.ViewHolder holder, int position) {
        FeedModal modal = feedModalArrayList.get(position);
//        holder.feed_author_name.setText(modal.getUsername());
//        if (modal.getMedia_type().equals("IMAGE")) {
//            Picasso.get().load(modal.getMedia_url()).into(holder.feed_image);
//        }
//        holder.feed_description.setText(modal.getCaption());
//        Picasso.get().load(modal.getAuthor_url()).into(holder.feed_author_avatar);
        Picasso.get().load(modal.getAuthor_avatar()).into(holder.feed_author_avatar);
        holder.feed_author_name.setText(modal.getAuthor_name());
        Picasso.get().load(modal.getImage()).into(holder.feed_image);
        holder.feed_description.setText(modal.getDescription());

    }

    @Override
    public int getItemCount() {
        return feedModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView feed_author_avatar;
        private TextView feed_author_name;
        private ImageView feed_image;
        private TextView feed_description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            feed_author_avatar = itemView.findViewById(R.id.feed_author_avatar);
            feed_author_name = itemView.findViewById(R.id.feed_author_name);
            feed_image = itemView.findViewById(R.id.feed_image);
            feed_description = itemView.findViewById(R.id.feed_description);
        }
    }
}
