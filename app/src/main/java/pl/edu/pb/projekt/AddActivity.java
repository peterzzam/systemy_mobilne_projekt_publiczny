package pl.edu.pb.projekt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class AddActivity extends AppCompatActivity {

    private Button takeAPhotoButton;
    private Button addFromUrlButton;
    private BottomNavigationView bottomNavigationView;
    private Intent intent;
    private Intent intent2;
    private Uri tempUri;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        takeAPhotoButton = findViewById(R.id.take_a_photo_button);
        addFromUrlButton = findViewById(R.id.add_from_url_button);
        bottomNavigationView = findViewById(R.id.bottom_bar);

        ActivityResultLauncher<Intent> takePhotoActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            tempUri = getImageUri(getApplicationContext(), photo);
                            path = getRealPathFromURI(tempUri);
                            path = "file://" + path;

                            intent2 = new Intent(getApplicationContext(), TakePhotoActivity.class);
                            Bundle b = new Bundle();
                            b.putString("path", path);
                            intent2.putExtras(b);
                            startActivity(intent2);
                        }
                    }
                });

        takeAPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent("android.media.action.IMAGE_CAPTURE");
                takePhotoActivityResultLauncher.launch(intent);

            }
        });



        addFromUrlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(view.getContext(), AddFromUrlActivity.class);
                startActivity(intent);
            }
        });

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home:
                        intent = new Intent(bottomNavigationView.getContext(), HomeActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                    case R.id.add:
                        break;
                    case R.id.profile:
                        intent = new Intent(bottomNavigationView.getContext(), ProfileActivity.class);
                        bottomNavigationView.getContext().startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }
}