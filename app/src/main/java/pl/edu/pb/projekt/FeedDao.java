package pl.edu.pb.projekt;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FeedDao {
    @Query("SELECT * FROM feed")
    List<Feed> getAll();

    @Query("SELECT * FROM feed WHERE fid IN (:feedIds)")
    List<Feed> loadAllByIds(int[] feedIds);

    @Query("SELECT * FROM feed WHERE author LIKE :first LIMIT 1")
    Feed findByName(String first);

    @Insert
    void insertAll(Feed... feeds);

    @Delete
    void delete(Feed feed);
}