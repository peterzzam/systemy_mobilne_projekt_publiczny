package pl.edu.pb.projekt;

public class FeedModal {
    // variables for storing data
    // of our recycler view item
    private int id;
    private String author_avatar;
    private String author_name;
    private String image;
    private String description;

    public String getAuthor_avatar() {
        return author_avatar;
    }

    public void setAuthor_avatar(String author_avatar) {
        this.author_avatar = author_avatar;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FeedModal(int id, String author_avatar, String author_name, String image, String description) {
        this.id = id;
        this.author_avatar = author_avatar;
        this.author_name = author_name;
        this.image = image;
        this.description = description;
    }
}