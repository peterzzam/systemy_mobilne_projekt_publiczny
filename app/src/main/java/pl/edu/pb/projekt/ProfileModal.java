package pl.edu.pb.projekt;

public class ProfileModal {
    // variables for storing data
    // of our recycler view item
    private int id;
    private String profile_avatar;
    private String profile_name;

    public String getProfile_avatar() {
        return profile_avatar;
    }

    public void setProfile_avatar(String profile_avatar) {
        this.profile_avatar = profile_avatar;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public ProfileModal(int id, String profile_avatar, String profile_name) {
        this.id = id;
        this.profile_avatar = profile_avatar;
        this.profile_name = profile_name;
    }
}